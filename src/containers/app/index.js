import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

import About from '../about';
import Home from '../home';
import Landing from '../landing';
import ServiceCenter from '../service-center';
import SaveApplication from '../save-application';
import VisaDetails from '../visa-details';
import ApplicantDetails from '../applicant-details';
import BookAppointment from '../book-appointment';
import BookSlot from '../book-slot';

const App = (props) => {
  const { lang } = props;

  return (
    <Router>
      <div>
        <main>
          <Route exact path="/" component={Home} />
          <Route exact path="/landing" component={Landing} />
          <Route exact path="/about-us" component={About} />
          <Route exact path="/service-center" component={ServiceCenter} />
          <Route exact path="/save" component={SaveApplication} />
          <Route exact path="/visa-details" component={VisaDetails} />
          <Route exact path="/applicant-details" component={ApplicantDetails} />
          <Route exact path="/book-appointment" component={BookAppointment} />
          <Route exact path="/book-slot" component={BookSlot} />
        </main>
        </div>
    </Router>
  );
}


export default App;
