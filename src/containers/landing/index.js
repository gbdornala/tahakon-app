import React, { Component } from 'react';
import Select from 'react-select';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import '../../styles/react-select.css';
import './landing.css';

import getLocales from '../../api/locale';
import { handleLocaleChange } from '../../modules/locale';

class Landing extends Component {
  constructor(props) {
    super(props);
    this.handleLocaleChange = this.handleLocaleChange.bind(this);
  }
  componentWillMount() {
    this.props.actions.getLocales();
  }

  handleLocaleChange(option) {
    this.props.history.push('/');
    this.props.actions.handleLocaleChange(option);
  }

  render() {
    const { actions, locales, lang } = this.props;
    return (
      <div>
        <div className="col-md-4 col-md-offset-4" style={{ marginTop: '80px'}}>
        <h1 className="landing-title" style={{ marginBottom: '40px' }}>VFS Tasheel </h1>
        <h3>Select your country</h3>
        <Select
          name="form-field-name"
          value={lang}
          clearable={false}
          onChange={(option) => this.handleLocaleChange(option)}
          options={locales.map((locale) => {
            return {
              value: locale.lang,
              label: locale.locale,
            }
          })}
          />
        </div>
      </div>
    );
  }
};
const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({
    getLocales,
    handleLocaleChange,
  }, dispatch),
});

const mapStateToProps = (state) => {
  return {
    locales: state.locale.locales,
    lang: state.locale.lang,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Landing);
