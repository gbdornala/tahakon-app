import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Recaptcha from 'react-recaptcha';

import Sidebar from '../../components/sidebar';
import Header from '../../components/header';
import { Col, Row, Button, FormControl, FormGroup, Radio, ControlLabel } from 'react-bootstrap';
import { getCountries } from '../../api/locale';
import './visa-details.css'

class VisaDetails extends Component {
  componentWillMount() {
    this.props.actions.getCountries();
  }
  render() {
    const { actions, countries } = this.props;

    return (
      <div>
        <Row>
          <Header />
        </Row>
        <div className="VisaDetails-main">
          <div className="VisaDetails-title">Applicant Details</div>
          <div className="Your-Booking-Appoint">Your Booking Appointment is for?*</div>
          
          <div className="row" style={{ marginTop: '30px'}}>
            <div className="col-md-3">
              <FormControl componentClass="select">
                <option value="business">Business Visa</option>
                <option value="work">Work Permit</option>
              </FormControl>
            </div>
            
            <div className="col-md-2">
              <FormControl componentClass="select">
                <option value="business">Individual</option>
                <option value="work">Family</option>
                <option value="work">Group</option>
              </FormControl>
            </div>
          </div>
          
          <div className="row" style={{ marginTop: '20px' }}>
            <div className="col-md-2 Your-Booking-Appoint">If family or group</div>
            <div className="col-md-1 col-md-offset-1">
              <FormGroup controlId="first-name">
                <ControlLabel>Children</ControlLabel>
                <FormControl componentClass="select">
                  <option value={1}>1</option>
                  <option value={2}>2</option>
                  <option value={3}>3</option>
                  <option value={4}>4</option>
                  <option value={5}>5</option>
                  <option value={6}>6</option>
                </FormControl>
              </FormGroup>
            </div>
            <div className="col-md-1 col-md-offset-1">
              <FormGroup controlId="first-name">
                <ControlLabel>Adults</ControlLabel>
                <FormControl componentClass="select">
                  <option value={1}>1</option>
                  <option value={2}>2</option>
                  <option value={3}>3</option>
                  <option value={4}>4</option>
                  <option value={5}>5</option>
                  <option value={6}>6</option>
                </FormControl>
              </FormGroup>
            </div>
          </div>
          
          <div className="row col-md-7" style={{ marginTop: '30px', marginBottom: '30px' }}>
            <div className="pull-right ">
              <Link  to="/applicant-details">
                <Button className="ServiceCenter-button" bsStyle="primary">Continue</Button>
              </Link>
            </div>
          </div>
          
        
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({
    getCountries,
  }, dispatch),
});

const mapStateToProps = (state) => {
  return {
    countries: state.locale.countries,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(VisaDetails);
