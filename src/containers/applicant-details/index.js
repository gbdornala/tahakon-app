import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Recaptcha from 'react-recaptcha';

import Sidebar from '../../components/sidebar';
import Header from '../../components/header';
import { Col, Row, Button, FormControl, FormGroup, Radio, ControlLabel } from 'react-bootstrap';
import { getCountries } from '../../api/locale';

class ApplicantDetails extends Component {
  componentWillMount() {
    this.props.actions.getCountries();
  }
  render() {
    const { actions, countries } = this.props;

    return (
      <div>
        <Row>
          <Header />
        </Row>
        <div style={{ margin: '0 120px'}}>
          <Row>
            
            <Col md="8">
              <div style={{ padding: '10px'}}>
                <div className="SaveApplication-title1">Home/Appointment/Schedule an Appointmen</div>
                <div className="SaveApplication-title2">Applicant Details</div>
                <div style={{ border: '1px solid #ebebeb', borderRadius: '4px', padding: '10px' }}>
                  <Row>
                    <Col md="5">
                      <FormGroup controlId="first-name">
                        <ControlLabel>First Name</ControlLabel>
                        <FormControl
                          type="text"
                          value=""
                          placeholder=""
                          onChange={() => {}}
                        />
                      </FormGroup>
                    </Col>
                    <Col md="5" mdOffset="1">
                    <FormGroup controlId="second-name">
                      <ControlLabel>Second Name</ControlLabel>
                      <FormControl
                        type="text"
                        value=""
                        placeholder=""
                        onChange={() => {}}
                      />
                    </FormGroup>
                    </Col>
                  </Row>

                  <Row>
                    <Col md="5">
                      <FormGroup controlId="other-name">
                        <ControlLabel>Other/G Father Name</ControlLabel>
                        <FormControl
                          type="text"
                          value=""
                          placeholder=""
                          onChange={() => {}}
                        />
                      </FormGroup>
                    </Col>
                    <Col md="5" mdOffset="1">
                    <FormGroup controlId="last-name">
                      <ControlLabel>Last /Family Name</ControlLabel>
                      <FormControl
                        type="text"
                        value=""
                        placeholder=""
                        onChange={() => {}}
                      />
                    </FormGroup>
                    </Col>
                  </Row>

                  <Row>
                    <Col md="5">
                      <FormGroup controlId="gender">
                        <ControlLabel>Gender</ControlLabel>
                        <FormControl componentClass="select">
                          <option value="male">Male</option>
                          <option value="female">Female</option>
                        </FormControl>
                      </FormGroup>
                    </Col>
                    <Col md="5" mdOffset="1">
                    <FormGroup controlId="marital-status">
                      <ControlLabel>Marital Status</ControlLabel>
                      <FormControl componentClass="select">
                        <option value="married">Married</option>
                        <option value="single">Single</option>
                      </FormControl>
                    </FormGroup>
                    </Col>
                  </Row>

                  <Row>
                    <Col md="5">
                      <FormGroup controlId="nationality">
                        <ControlLabel>Nationality</ControlLabel>
                        <FormControl componentClass="select">
                          <option value="india">India</option>
                          <option value="germany">Germany</option>
                          <option value="spain">Spain</option>
                        </FormControl>
                      </FormGroup>
                    </Col>
                    <Col md="5" mdOffset="1">
                    <FormGroup controlId="religion">
                      <ControlLabel>Religion</ControlLabel>
                      <FormControl componentClass="select">
                        <option value="islam">Islam</option>
                        <option value="christian">Christianity</option>
                      </FormControl>
                    </FormGroup>
                    </Col>
                  </Row>

                  <Row>
                    <Col md="5">
                      <FormGroup controlId="occupation">
                        <ControlLabel>Occupation</ControlLabel>
                        <FormControl
                          type="text"
                          value=""
                          placeholder=""
                          onChange={() => {}}
                        />
                      </FormGroup>
                    </Col>
                    <Col md="5" mdOffset="1">
                    <FormGroup controlId="qualification">
                      <ControlLabel>Qualification</ControlLabel>
                      <FormControl
                        type="text"
                        value=""
                        placeholder=""
                        onChange={() => {}}
                      />
                    </FormGroup>
                    </Col>
                  </Row>

                  <Row>
                    <Col md="5">
                      <FormGroup controlId="source-of-degree">
                        <ControlLabel>Source of Degree</ControlLabel>
                        <FormControl
                          type="text"
                          value=""
                          placeholder=""
                          onChange={() => {}}
                        />
                      </FormGroup>
                    </Col>
                    <Col md="5" mdOffset="1">
                    <FormGroup controlId="mahram">
                      <ControlLabel>Mahram Relation</ControlLabel>
                      <FormControl
                        type="text"
                        value=""
                        placeholder=""
                        onChange={() => {}}
                      />
                    </FormGroup>
                    </Col>
                  </Row>

                  <Row>
                    <Col md="5">
                      <FormGroup controlId="mahram-name">
                        <ControlLabel>Mahram Name</ControlLabel>
                        <FormControl
                          type="text"
                          value=""
                          placeholder=""
                          onChange={() => {}}
                        />
                      </FormGroup>
                    </Col>
                    <Col md="5" mdOffset="1">
                    <FormGroup controlId="contact">
                      <ControlLabel>Contact Number</ControlLabel>
                      <FormControl
                        type="text"
                        value=""
                        placeholder=""
                        onChange={() => {}}
                      />
                    </FormGroup>
                    </Col>
                  </Row>

                  <Row>
                    <Col md="5">
                      <FormGroup controlId="email">
                        <ControlLabel>Valid Email</ControlLabel>
                        <FormControl
                          type="text"
                          value=""
                          placeholder=""
                          onChange={() => {}}
                        />
                      </FormGroup>
                    </Col>
                    <Col md="5" mdOffset="1">
                    <FormGroup controlId="saudi-mission">
                      <ControlLabel>Saudi Mission In</ControlLabel>
                      <FormControl componentClass="select">
                        <option value="algiers">Algiers</option>
                      </FormControl>
                    </FormGroup>
                    </Col>
                  </Row>

                  <Row>
                    <Col md="5">
                      <FormGroup controlId="port-of-entry">
                        <ControlLabel>Port of Entry</ControlLabel>
                        <FormControl componentClass="select">
                          <option value="riyadh">Riyadh</option>
                          <option value="dammam">Dammam</option>
                        </FormControl>
                      </FormGroup>
                    </Col>
                    <Col md="5" mdOffset="1">
                    <FormGroup controlId="transport-mode">
                      <ControlLabel>Transport Mode</ControlLabel>
                      <FormControl componentClass="select">
                        <option value="car">Car</option>
                        <option value="bus">Bus</option>
                        <option value="train">Train</option>
                      </FormControl>
                    </FormGroup>
                    </Col>
                  </Row>

                  <Col md="2">
                    <div className="pull-left" style={{ padding: '20px 0', marginLeft: '40px', }}>
                      <Link to="/visa-details">
                        <Button  width="100px"  bsStyle="primary">Back</Button>
                      </Link>
                    </div>
                  </Col>
                  <Col md="2" mdOffset={8}>
                    <div className="pull-right" style={{ padding: '20px 0', marginRight: '40px', width: '100px' }}>
                      <Link to="/book-appointment">
                        <Button  bsStyle="primary">Continue</Button>
                      </Link>
                    </div>
                  </Col>


                </div>
              </div>
            </Col>
            
          </Row>
          </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({
    getCountries,
  }, dispatch),
});

const mapStateToProps = (state) => {
  return {
    countries: state.locale.countries,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ApplicantDetails);
