import React from 'react';
import { Table } from 'react-bootstrap';

const VisaCenterTable = (props) => {
  const { serviceCenters } = props;
  if (!serviceCenters.length) {
    return null;
  }


  return (
    <Table striped bordered condensed hover>
      <thead>
        <tr>
          <th>#</th>
          <th>City</th>
          <th>Center</th>
          <th>Normal</th>
          <th>Lounge</th>
        </tr>
      </thead>
      <tbody>
      {
        serviceCenters.map((center) => {
          return (
          <tr>
            <td>{center.id}</td>
            <td>{center.city}</td>
            <td>{center.center}</td>
            <td>{center.normal}</td>
            <td>{center.lounge}</td>
          </tr>
        )
      })
      }
      </tbody>
    </Table>
  )
};

export default VisaCenterTable;
