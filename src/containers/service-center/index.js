import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Select from 'react-select';
import Recaptcha from 'react-recaptcha';


import Header from '../../components/header';
import Footer from '../../components/footer';
import ProgressBar from '../../components/progress-bar';
import VisaCenterTable from './table';
import { Col, Row, Button } from 'react-bootstrap';
import './service-center.css'
import { getCountries, getVisaCategories, getServiceCenters } from '../../api/locale';
import { setVisaCategory, setNationality } from '../../modules/appStatus';

import { FormControl } from 'react-bootstrap';

class ServiceCenter extends Component {
  componentWillMount() {
    this.props.actions.getCountries();
    this.props.actions.getVisaCategories();
  }
  handleVisaCategoryChange = (option) => {
    this.props.actions.getServiceCenters();
    this.props.actions.setVisaCategory(option);
  }
  render() {
    const { actions, countries, visaCategories, visaCategorySelected, nationalitySelelected, serviceCenters } = this.props;

    return (
      <div className="ServiceCenter">
        <Header />
        <div className="ServiceCenter-main">
        
          <div className="row" style={{ marginTop: '30px' }}>
            <div className="col-md-3 col-md-offset-1 ServiceCenter-lable">
              Nationality
            </div>
            <div className="col-md-3">
              <Select
                name="form-field-name"
                value={nationalitySelelected}
                placeholder="Select Nationality"
                clearable={false}
                onChange={actions.setNationality}
                options={countries.map((country) => {
                  return {
                    value: country,
                    label: country,
                  }
                })}
              />
              </div>
            </div>
            
            <div className="row" style={{ marginTop: '30px' }}>
              <div className="col-md-3 col-md-offset-1 ServiceCenter-lable">
                Visa Category
              </div>
              <div className="col-md-3">
                <Select
                  name="form-field-name"
                  value={visaCategorySelected}
                  placeholder="Select visa category"
                  clearable={false}
                  onChange={(option) => this.handleVisaCategoryChange(option)}
                  options={visaCategories.map((c) => {
                    return {
                      value: c.id,
                      label: c.type,
                    }
                  })}
                  />
                </div>
              </div>
              
              
              <div className="row" style={{ marginTop: '30px' }}>
                <div className="col-md-3 col-md-offset-1 ServiceCenter-lable">
                  Passport Number
                </div>
                <div className="col-md-3">
                  <FormControl
                    type="text"
                    value=""
                    placeholder="Passport Number"
                    onChange={() => {}}
                  />
                </div>
              </div> 
              
              <div className="row" style={{ marginTop: '30px' }}>
                <div className="col-md-6 col-md-offset-1">
                  <Recaptcha
                    sitekey="xxxxxxxxxxxxxxxxxxxx"
                    render="explicit"
                    onloadCallback={() => {}}
                  />
                </div>
              </div>
                 
              <div className="row col-md-6" style={{ marginTop: '30px', marginBottom: '30px' }}>
                <div className="pull-right ">
                  <Link  to="/visa-details">
                    <Button className="ServiceCenter-button" bsStyle="primary">Continue</Button>
                  </Link>
                </div>
              </div>
          </div>
          <Footer />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({
    getCountries,
    getVisaCategories,
    getServiceCenters,
    setVisaCategory,
    setNationality,
  }, dispatch),
});

const mapStateToProps = (state) => {
  return {
    countries: state.locale.countries,
    visaCategories: state.locale.visaCategories,
    visaCategorySelected: state.appStatus.visaCategorySelected,
    nationalitySelelected: state.appStatus.nationalitySelelected,
    serviceCenters: state.locale.serviceCenters,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ServiceCenter);;
