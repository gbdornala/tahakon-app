import React, { Component } from 'react'
import { connect } from 'react-redux';

import { FormattedMessage } from 'react-intl';
import '../../main.js';

import Footer from '../../components/footer';
import logo from '../../images/home/group-11.svg';
import auEnglish from '../../images/home/rectangle-2-copy-2.png';
import nextButton from '../../images/home/rectangle-12-copy-7.svg';
import './home.css';
import { Button} from 'react-bootstrap';

class Home extends Component {
  componentWillMount() {
    if (!this.props.lang) {
      this.props.history.push('/landing')
    }
  }
  handleButtonClick = () => {
    this.props.history.push('/service-center')
  }
  render() {
    return (
      <div className="Home">
        <div className="Home-image">
          <div className="">
            <img src={logo} />
            <div className="pull-right">
              <img src={auEnglish} className="Rectangle-2-Copy-2" />
              <span className="AU---English">AU - English</span>
            </div>
          </div>
          <div style={{ marginTop: '521px' }} onClick={this.handleButtonClick}>
            <Button className="Rectangle-12-Copy-7" >
              Book Now
            </Button>
          </div>
        </div>
        <div>
        </div>
        <Footer />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  lang: state.locale.lang,
})


export default connect(
  mapStateToProps,
  null
)(Home)
