import React, { Component } from 'react'
import { connect } from 'react-redux';
import {
  increment,
  incrementAsync,
  decrement,
  decrementAsync
} from '../../modules/counter';
import {FormattedMessage, FormattedHTMLMessage} from 'react-intl';
import '../../main.js';

import Header from '../header';

class Home extends Component {
  componentWillMount() {
    if (!this.props.lang) {
      this.props.history.push('/landing')
    }
  }
  render() {
    return (
      <div>
        <Header />
        <section className="featured">
          <div className="container">
            <div className="row mar-bot40">
              <div className="col-md-6 col-md-offset-3">

                <div className="align-center">
                  <i className="fa fa-flask fa-5x mar-bot20"></i>
                  <h2 className="slogan">
                    <FormattedMessage
                      id="app.title"
                      defaultMessage="Hello" />
                  </h2>
                  <p>
                    Lorem ipsum dolor sit amet, natum bonorum expetendis usu ut. Eum impetus offendit disputationi eu, at vim aliquip lucilius praesent. Alia laudem antiopam te ius, sed ad munere integre, ubique facete sapientem nam ut.

                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section id="section-services" className="section pad-bot30 bg-white">
          <div className="container">
            <div className="row mar-bot40">
              <div className="col-lg-4">
                <div className="align-center">
                  <i className="fa fa-calendar fa-5x mar-bot20"></i>
                  <h4 className="text-bold">Schedule Appointment</h4>
                  <p>Lorem ipsum dolor sit amet, mutat graeco volumus ad eam, singulis patrioque comprehensam nam no. Mei cu dicat voluptaria volumus.
                  </p>
                </div>
              </div>

              <div className="col-lg-4">
                <div className="align-center">
                  <i className="fa fa-search fa-5x mar-bot20"></i>
                  <h4 className="text-bold">Track Status</h4>
                  <p>Lorem ipsum dolor sit amet, mutat graeco volumus ad eam, singulis patrioque comprehensam nam no. Mei cu dicat voluptaria volumus.
                  </p>
                </div>
              </div>

              <div className="col-lg-4">
                <div className="align-center">
                  <i className="fa fa-file-text-o fa-5x mar-bot20"></i>
                  <h4 className="text-bold">Retreive Application</h4>
                  <p>Lorem ipsum dolor sit amet, mutat graeco volumus ad eam, singulis patrioque comprehensam nam no. Mei cu dicat voluptaria volumus.
                  </p>
                </div>
              </div>
            </div>

          </div>
        </section>

        <section id="testimonials" className="section" data-stellar-background-ratio="0.5">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="align-center">
                  <div className="testimonial pad-top40 pad-bot40 clearfix">
                    <h5>
                              Nunc velit risus, dapibus non interdum quis, suscipit nec dolor. Vivamus tempor tempus mauris vitae fermentum. In vitae nulla lacus. Sed sagittis tortor vel arcu sollicitudin nec tincidunt metus suscipit.Nunc velit risus, dapibus non interdum.
                            </h5>
                    <br/>
                    <span className="author">&mdash; MIKE DOE <a href="#">www.siteurl.com</a></span>
                  </div>

                </div>
              </div>

            </div>
          </div>
        </section>

        <section id="footer" className="section footer">
          <div className="container">
            <div className="row animated opacity mar-bot20" data-andown="fadeIn" data-animation="animation">
              <div className="col-sm-12 align-center">
                <ul className="social-network social-circle">
                  <li><a href="#" className="icoRss" title="Rss"><i className="fa fa-rss"></i></a></li>
                  <li><a href="#" className="icoFacebook" title="Facebook"><i className="fa fa-facebook"></i></a></li>
                  <li><a href="#" className="icoTwitter" title="Twitter"><i className="fa fa-twitter"></i></a></li>
                  <li><a href="#" className="icoGoogle" title="Google +"><i className="fa fa-google-plus"></i></a></li>
                  <li><a href="#" className="icoLinkedin" title="Linkedin"><i className="fa fa-linkedin"></i></a></li>
                </ul>
              </div>
            </div>

            <div className="row align-center copyright">
              <div className="col-sm-12">
                <p>Copyright &copy; Amoeba</p>
                <div className="credits">
                  Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
                </div>
              </div>
            </div>
          </div>

        </section>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  lang: state.locale.lang,
})


export default connect(
  mapStateToProps,
  null
)(Home)
