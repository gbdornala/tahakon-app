import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Recaptcha from 'react-recaptcha';

import Sidebar from '../../components/sidebar';
import Header from '../../components/header';
import AvailableSlots from './available-slots';
import { Col, Row, Button, FormControl, FormGroup, Radio, ControlLabel } from 'react-bootstrap';
import { getSlots } from '../../api/booking';
import './book-slot.css';

class BookSlot extends Component {
  componentWillMount() {
    this.props.actions.getSlots();
  }
  render() {
    const { actions, slots } = this.props;

    return (
      <div style={{ margin: '100px 100px 0 100px' }}>
        <Row>
          <Header />
        </Row>
        <div>
          <Row>
            <Col md="2">
              <Sidebar />
            </Col>
            <Col md="8">
              <div style={{ padding: '10px'}}>
                <div className="SaveApplication-title1">Home/Appointment/Schedule an Appointmen</div>
                <h3 className="SaveApplication-title2">Book Slot</h3>
                <div>Select the slot from available slots</div>
                <AvailableSlots slots={slots}/>
                <div style={{ padding: '10px' }}>
                  <Col md="2">
                    <div className="pull-left" style={{ padding: '20px 0', marginLeft: '40px', }}>
                      <Link to="/visa-details">
                        <Button  width="100px"  bsStyle="primary">Back</Button>
                      </Link>
                    </div>
                  </Col>
                  <Col md="2" mdOffset={8}>
                    <div className="pull-right" style={{ padding: '20px 0', marginRight: '40px', width: '100px' }}>
                      <Link to="/book-slot">
                        <Button  bsStyle="primary">Continue</Button>
                      </Link>
                    </div>
                  </Col>

                </div>
              </div>
            </Col>
            <Col md="2">
              <Sidebar />
            </Col>
          </Row>
          </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({
    getSlots,
  }, dispatch),
});

const mapStateToProps = (state) => {
  return {
    slots: state.booking.slots,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BookSlot);
