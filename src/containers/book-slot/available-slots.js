import React, { Component } from 'react';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';

import { Col, Row, Button, FormControl, FormGroup, Radio, ControlLabel } from 'react-bootstrap';

class AvailableSlots extends Component {

  renderDate(slot) {
    return (
        <li className="AvailableSlots-slot">
          <div>{slot.slotTime}</div>
          {
            slot.available
            ? <i className="fa fa-times" aria-hidden="true"></i>
            : <input type="radio" value={true} />
          }
        </li>
    )
  }

  render() {
    const { slots } = this.props;
    const halfWay = slots.length / 2 - 1;
    const row1Slots = slots.slice(0, halfWay);
    const row2Slots = slots.slice(halfWay + 1, slots.length - 1);

    return (
      <div style={{ border: '1px solid #ddd' }}>
        <ul>
          <div className="row">
            {row1Slots.map((slot) => this.renderDate(slot))}
          </div>
          <div className="row">
            {row2Slots.map((slot) => this.renderDate(slot))}
          </div>
        </ul>
      </div>
    );
  }
}

export default AvailableSlots;
