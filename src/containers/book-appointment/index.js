import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Recaptcha from 'react-recaptcha';

import Sidebar from '../../components/sidebar';
import Header from '../../components/header';
import ScheduleCalender from './schedule-calender.js';
import { Col, Row, Button, FormControl, FormGroup, Radio, ControlLabel } from 'react-bootstrap';
import { getDates } from '../../api/booking';
import './book-appointment.css';

class BookAppointment extends Component {
  componentWillMount() {
    this.props.actions.getDates();
  }
  render() {
    const { actions, dates } = this.props;

    return (
      <div style={{ margin: '100px 100px 0 100px' }}>
        <Row>
          <Header />
        </Row>
        <div>
          <Row>
            <Col md="2">
              <Sidebar />
            </Col>
            <Col md="8">
              <div style={{ padding: '10px'}}>
                <div className="SaveApplication-title1">Home/Appointment/Schedule an Appointmen</div>
                <div className="row">
                  <h3 className="col-md-4 SaveApplication-title2">Book Appointment</h3>
                  <h4 className="col-md-4 BookAppointment-price">Normal Fee BHD 46.50</h4>
                  <h4 className="col-md-4 BookAppointment-price">Lounges Fee BHD 54.50</h4>
                  <p>Select the visa center, appointment date and timeslot</p>
                </div>
                <ScheduleCalender dates={dates}/>
                <div style={{ padding: '10px' }}>

                  <Col md="2">
                    <div className="pull-left" style={{ padding: '20px 0', marginLeft: '40px', }}>
                      <Link to="/visa-details">
                        <Button  width="100px"  bsStyle="primary">Back</Button>
                      </Link>
                    </div>
                  </Col>
                  <Col md="2" mdOffset={8}>
                    <div className="pull-right" style={{ padding: '20px 0', marginRight: '40px', width: '100px' }}>
                      <Link to="/book-slot">
                        <Button  bsStyle="primary">Continue</Button>
                      </Link>
                    </div>
                  </Col>

                </div>
              </div>
            </Col>
            <Col md="2">
              <Sidebar />
            </Col>
          </Row>
          </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({
    getDates,
  }, dispatch),
});

const mapStateToProps = (state) => {
  return {
    dates: state.booking.dates,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BookAppointment);
