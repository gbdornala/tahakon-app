import React, { Component } from 'react';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';

import { Col, Row, Button, FormControl, FormGroup, Radio, ControlLabel } from 'react-bootstrap';
import './book-appointment.css';

class ScheduleCalender extends Component {

  renderAvailability(date) {
    if (!isEmpty(date.holiday) && date.holiday.isHoliday) {
      return <div>{date.holiday.holidayDetail}</div>
    }

    if (date.weekend) {
      return <div>Weekly Off</div>
    }
    return (
      <div>
        <input type="checkbox" name="vehicle" value="Bike" />
        <input type="checkbox" name="vehicle" value="Bike" />
      </div>
    )
  }

  renderDate(date) {
    return (
      <Col md={2}>
        <div>
          <div>{date.date}</div>
          <div>{date.day}</div>
        </div>
        <div>
          {this.renderAvailability(date)}
        </div>
      </Col>
    )
  }

  render() {
    const { dates } = this.props;
    console.log(dates)

    return (
      <div style={{ border: '1px solid #ddd' }}>
        <Row>
          {dates.map((date) => this.renderDate(date))}
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    countries: state.locale.countries,
  };
};

export default connect(mapStateToProps, null)(ScheduleCalender);
