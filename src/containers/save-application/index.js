import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Recaptcha from 'react-recaptcha';

import Sidebar from '../../components/sidebar';
import Header from '../../components/header';
import { Col, Row, Button, Checkbox, FormControl } from 'react-bootstrap';
import { getCountries } from '../../api/locale';

class SaveApplication extends Component {
  componentWillMount() {
    this.props.actions.getCountries();
  }
  render() {
    const { actions, countries } = this.props;

    return (
      <div style={{ margin: '100px 100px 0 100px' }}>
        <Row>
          <Header />
        </Row>
        <div>
          <Row>
            <Col md="2">
              <Sidebar />
            </Col>
            <Col md="10">
              <div style={{ padding: '10px'}}>
                <div className="SaveApplication-title1">Home/Schedule an Appointmen</div>
                <div className="SaveApplication-title2">Save my application details</div>
                <p>You can retrieve your application details anytime by providing the below details:</p>
                <Row>
                  <Col md="3">
                    <div>Email</div>
                    <div style={{ margin:"10px 10px 10px 0"}}>
                      <FormControl
                         type="text"
                         value=""
                         placeholder="Enter email"
                         onChange={() => {}}
                       />
                    </div>
                  </Col>
                  <Col md="3">
                    <div>Passport Number</div>
                    <div style={{ margin:"10px 10px 10px 0" }}>
                      <FormControl
                         type="text"
                         value=""
                         placeholder="Enter Passport Number"
                         onChange={() => {}}
                       />
                    </div>
                    <Row>
                      <Col md="4">
                        <Recaptcha
                          sitekey="xxxxxxxxxxxxxxxxxxxx"
                          render="explicit"
                          onloadCallback={() => {}}
                        />
                      </Col>
                    </Row>
                  </Col>
                </Row>
                </div>
            </Col>
          </Row>

          <div className="pull-right" style={{ padding: '20px 0' }}>
            <Link to="/visa-details">
              <Button  bsStyle="primary">Continue</Button>
            </Link>
          </div>
          </div>

      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({
    getCountries,
  }, dispatch),
});

const mapStateToProps = (state) => {
  return {
    countries: state.locale.countries,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SaveApplication);
