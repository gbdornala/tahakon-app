import {
  addDataToStore,
  addCountriesToStore,
  addVisaCategoriesToStore,
  addServiceCentersToStore,
 } from '../../modules/locale';
import axios from 'axios';

const getLocales = () => {
  return (dispatch) => {
    return axios.get('http://localhost:3000/locales')
      .then((response) => {
        dispatch(addDataToStore(response.data));
      })
      .catch((error) => {
        throw error;
      });
  };
};

const getCountries = () => {
  return (dispatch) => {
    return axios.get('http://localhost:3000/countries')
      .then((response) => {
        dispatch(addCountriesToStore(response.data));
      })
      .catch((error) => {
        throw error;
      });
  };
};

const getVisaCategories = () => {
  return (dispatch) => {
    return axios.get('http://localhost:3000/visa-categories')
      .then((response) => {
        dispatch(addVisaCategoriesToStore(response.data));
      })
      .catch((error) => {
        throw error;
      });
  };
};

const getServiceCenters = () => {
  return (dispatch) => {
    return axios.get('http://localhost:3000/service-centers')
      .then((response) => {
        dispatch(addServiceCentersToStore(response.data));
      })
      .catch((error) => {
        throw error;
      });
  };
};

export default getLocales;

export {
  getCountries,
  getVisaCategories,
  getServiceCenters,
}
