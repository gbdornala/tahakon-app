import { addDatesToStore, addSlotsToStore } from '../../modules/booking';
import axios from 'axios';

const getDates = () => {
  return (dispatch) => {
    return axios.get('http://localhost:3000/dates')
      .then((response) => {
        dispatch(addDatesToStore(response.data));
      })
      .catch((error) => {
        throw error;
      });
  };
};

const getSlots = () => {
  return (dispatch) => {
    return axios.get('http://localhost:3000/slots')
      .then((response) => {
        dispatch(addSlotsToStore(response.data));
      })
      .catch((error) => {
        throw error;
      });
  };
};

export {
  getDates,
  getSlots,
}
