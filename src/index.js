import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux'
import store, { history } from './store'
import App from './containers/app';
import ConnectedIntlProvider from './containers/intl/ConnectedIntlProvider';

import { addLocaleData } from "react-intl";
import locale_en from 'react-intl/locale-data/en';
import locale_de from 'react-intl/locale-data/de';

addLocaleData([...locale_en, ...locale_de]);

const target = document.querySelector('#root')

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <ConnectedIntlProvider>
        <App />
      </ConnectedIntlProvider>
    </ConnectedRouter>
  </Provider>,
  target
);
