import { handleActions, createAction } from 'redux-actions';

const SET_NATIONALITY = 'SET_NATIONALITY';
const SET_VISACATEGORY = 'SET_VISACATEGORY';


const setNationality = createAction(SET_NATIONALITY);
const setVisaCategory = createAction(SET_VISACATEGORY);

const initialState = {
  nationalitySelelected: '',
  visaCategorySelected: ','
}

const appStatus = handleActions({
  [SET_NATIONALITY]: (state, { payload }) => {
    return {
      ...state,
      nationalitySelelected: payload,
    }
  },
  [SET_VISACATEGORY]: (state, { payload }) => {
    return {
      ...state,
      visaCategorySelected: payload.value,
    }
  },
}, initialState);

export default appStatus;

export {
  setNationality,
  setVisaCategory
};
