import { handleActions, createAction } from 'redux-actions';

const ADD_DATES_DATA = 'ADD_DATES_DATA';
const ADD_SLOTS_TO_STORE = 'ADD_SLOTS_TO_STORE';

const addDatesToStore = createAction(ADD_DATES_DATA);
const addSlotsToStore = createAction(ADD_SLOTS_TO_STORE);

const initialState = {
  dates: [],
  slots: [],
}

const locale = handleActions({
  [ADD_DATES_DATA]: (state, { payload }) => {
    return {
      ...state,
      dates: payload,
    }
  },
  [ADD_SLOTS_TO_STORE]: (state, { payload }) => {
    return {
      ...state,
      slots: payload
    }
  }
}, initialState);

export default locale;

export {
  addDatesToStore,
  addSlotsToStore,
};
