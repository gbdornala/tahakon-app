
import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import counter from './counter';
import locale from './locale';
import booking from './booking';
import appStatus from './appStatus';

export default combineReducers({
  routing: routerReducer,
  counter,
  locale,
  booking,
  appStatus,
})
