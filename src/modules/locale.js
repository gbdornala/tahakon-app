import { handleActions, createAction } from 'redux-actions';

import messages_de from "../translations/de.json";
import messages_en from "../translations/en.json";

const ADD_COUNTRIES_DATA = 'ADD_COUNTRIES_DATA';
const ADD_LOCALES_DATA = 'ADD_LOCALES_DATA';
const SET_LOCALE = 'SET_LOCALE';
const ADD_VISACATEGORIES_DATA = 'ADD_VISACATEGORIES_DATA';
const ADD_SERVICECENTERS = 'ADD_SERVICECENTERS';

const addDataToStore = createAction(ADD_LOCALES_DATA);
const addCountriesToStore = createAction(ADD_COUNTRIES_DATA);
const handleLocaleChange = createAction(SET_LOCALE);
const addVisaCategoriesToStore = createAction(ADD_VISACATEGORIES_DATA);
const addServiceCentersToStore = createAction(ADD_SERVICECENTERS);

const initialState = {
  lang: '',
  countries: [],
  locales: [],
  visaCategories: [],
  serviceCenters: [],
  messages: {
      'de': messages_de,
      'en': messages_en
  },
}

const locale = handleActions({
  [ADD_COUNTRIES_DATA]: (state, { payload }) => {
    return {
      ...state,
      countries: payload,
    }
  },
  [ADD_LOCALES_DATA]: (state, { payload }) => {
    return {
      ...state,
      locales: payload,
    }
  },
  [SET_LOCALE]: (state, { payload }) => {
    console.log(payload)
    return {
      ...state,
      lang: payload.value,
      locale: payload.label,
    }
  },
  [ADD_VISACATEGORIES_DATA]: (state, { payload }) => {
    return {
      ...state,
      visaCategories: payload,
    }
  },
  [ADD_SERVICECENTERS]: (state, { payload }) => {
    return {
      ...state,
      serviceCenters: payload,
    }
  }
}, initialState);

export default locale;

export {
  addDataToStore,
  addCountriesToStore,
  addVisaCategoriesToStore,
  addServiceCentersToStore,
  handleLocaleChange,
};
