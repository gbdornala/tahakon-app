import React from 'react'

import './progress-bar.css';

const ProgressBar = (props) => {
  return (
    <div>
        <div className="timeline">
          <div className="entry">
            <h1 style={{ color: 'blue'}}>1990</h1>
          </div>
          <div className="entry">
            <h1>2000</h1>
          </div>
          <div className="entry">
            <h1>2005</h1>
          </div>
        <div className="entry">
          <h1>2010</h1>
        </div>
      </div>
    </div>
  );
}


export default ProgressBar;
