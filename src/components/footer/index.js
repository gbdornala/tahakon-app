import React from 'react'

import './footer.css';
import logo from '../../images/footer/group-41-copy.svg';
import locationIcon from '../../images/footer/group-copy-14.svg';
import facebookIcon from '../../images/footer/group-34.svg';
import linkedinIcon from '../../images/footer/group-35.svg';
import twitterIcon from '../../images/footer/group-33.svg'
import commentIcon from '../../images/footer/group-copy-12.svg';
import envelop from '../../images/footer/group-copy-12.svg';
import phoneIcon from '../../images/footer/bitmap-copy-4.jpg';

const Footer = (props) => {
  return (
    <div className="Footer">
      <img src={logo} className="Footer-logo" ></img>
      <div className="row Footer-social ">
        <img src={facebookIcon} className="Group-34 col-md-4"></img>
        <img src={linkedinIcon} className="Group-35 col-md-4" ></img>
        <img src={twitterIcon} className="Group-33 col-md-4"></img>
     </div>

     <div className="Footer-details" >
        <div className="Footer-sub-details" >
          <div className="Footer-textHeader">Get in Touch</div>
          <div className="Footer-item">
            <img src={locationIcon} className="Group-Copy-14"></img>
            <span className="Footer-text1">SVC Locator</span>
          </div>
          <div className="Footer-item">
            <img src={commentIcon} className="Group-Copy-12" />
            <span className="Footer-text1">Live chat support</span>
          </div>
          <div className="Footer-item">
            <img src={envelop} className="Group-Copy-12" />
            <span className="Footer-text1">Contact us</span>
          </div>
          <div className="Footer-item">
            <img src={phoneIcon} className="Group-Copy-12" />
            <span className="Footer-text1">+ 900 234 2345</span>
          </div>
        </div>

        <div className="Footer-sub-details" >
          <div className="Footer-textHeader">Book Appointment</div>
          <div className="Footer-item">
            <span className="Footer-text1">schedule appoinement</span>
          </div>
          <div className="Footer-item">
            <span className="Footer-text1">Visa Fees</span>
          </div>
          <div className="Footer-item">
            <span className="Footer-text1">Documents required</span>
          </div>
          <div className="Footer-item">
            <span className="Footer-text1">Guidelines</span>
          </div>
        </div>

        <div className="Footer-sub-details" >
          <div className="Footer-textHeader">Manage Appointment</div>
          <div className="Footer-item">
            <span className="Footer-text1">Retrieve Appointment</span>
          </div>
          <div className="Footer-item">
            <span className="Footer-text1">Re-Schedule Appointment</span>
          </div>
          <div className="Footer-item">
            <span className="Footer-text1">Waitlist</span>
          </div>
          <div className="Footer-item">
            <span className="Footer-text1">Guidelines</span>
          </div>
        </div>

        <div className="Footer-sub-details" >
          <div className="Footer-textHeader">Track Appointment</div>
        </div>
     </div>
    </div>
  );
}



export default Footer;
