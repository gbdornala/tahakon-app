import React from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';
import './sidebar.css';
import '../../main.js'

const Sidebar = (props) => {

  return (
    <ButtonGroup vertical>
      <Button className="sidebar-button">Schedule An Appointment</Button>
      <Button className="sidebar-button">Modify Information</Button>
      <Button className="sidebar-button">Reschedule An Appointment</Button>
      <Button className="sidebar-button">Cancel An Appointment</Button>
      <Button className="sidebar-button">Reprint Letter</Button>
      <Button className="sidebar-button">Retreive Application</Button>
    </ButtonGroup>
  );
}

export default Sidebar;
