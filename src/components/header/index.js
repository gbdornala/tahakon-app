import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import './header.css';

import logo from '../../images/header/group.png';
import au1 from '../../images/header/rectangle-2-copy-2.png';


const Header = (props) => (
  <div className="Header">
      <img src={logo} className="Group" />
      <div className="Header-menu pull-right">
        <span className="Header-menuItem How-to-Book ">How to Book</span>
        <span className="Header-menuItem Book-an-Appointment">Book an Appointment</span>
        <span className="Header-menuItem Manage-Appointment">Manage Appointment</span>
        <span className="Header-menuItem">
          <img src={au1} className="Rectangle-2-Copy-2" />
          <span className="AU---English-header">AU - English</span>
        </span>
      </div>
  </div>
);

const mapStateToProps = state => ({
  locale: state.locale.locale,
})


export default connect(mapStateToProps,null)(Header)
